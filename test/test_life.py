from algorithms import life


def test_increment():
    assert life.increment(1) == 2


def test_decrement():
    assert life.decrement(1) == 0
