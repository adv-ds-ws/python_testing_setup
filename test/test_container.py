from commons.container import Human


def test_human():

    new_human = Human()

    assert type(new_human).__name__ == "Human"

    assert new_human.age == 0

    years = 10

    for i in range(years):
        new_human.live()

    assert new_human.age == years
