def increment(number):
    return number + 1


def decrement(number):
    return number - 1


def time_to_die(age):
    return age > 65
