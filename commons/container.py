from algorithms import life


class Human():

    def __init__(self):
        self.age = 0
        print(f"I was born and I am {self.age} years old.")

    def live(self):
        self.age = life.increment(self.age)
        print(f"""Yet a another year has passed.
        I am now {self.age} years old.""")
        is_dead = life.time_to_die(self.age)
        if is_dead:
            print("My time has come...")
            del self
